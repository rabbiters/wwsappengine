package org.cpe22.wws.appengine;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

@SuppressWarnings("serial")
public class AddEvent extends HttpServlet {

	private PrintWriter out;

	private DatastoreService data = DatastoreServiceFactory.getDatastoreService();
	
	private String eventID = null;
	private String title = null;
	private String creatorUID = null;
	private String name = null;
	private String date = null;
	private String time = null;
	private String description = null;
	
	private String lat = null;
	private String lon = null;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		out = resp.getWriter();
		resp.setContentType("text/html");
		
		out.write("<center><h1>Add Event</h1></center><br>");
		out.write("<center>Add event via mobile application only</center><br>");
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		out = resp.getWriter();
		resp.setContentType("text/html");

		eventID = req.getParameter("eventID");
		title = req.getParameter("title");
		creatorUID = req.getParameter("creatorUID");
		name = req.getParameter("name");
		date = req.getParameter("date");
		time = req.getParameter("time");
		description = req.getParameter("description");
		lat = req.getParameter("lat");
		lon = req.getParameter("lon");

		if(eventID!= null && title!=null && creatorUID!=null && name!=null && date!=null && time!=null && description!=null && lat!=null && lon!=null){
			/*add into event table*/
			Entity en = new Entity("event");
			en.setProperty("eventID", eventID);
			en.setProperty("title", title);
			en.setProperty("creatorUID", creatorUID);
			en.setProperty("name", name);
			en.setProperty("date", date);
			en.setProperty("time", time);
			en.setProperty("description", description);
			en.setProperty("started", 0);
			data.put(en);
			
			/*add into event line item*/
			en = new Entity("eventLineItem");
			en.setProperty("eventID", eventID);
			en.setProperty("uid", creatorUID);
			en.setProperty("name", name);
			en.setProperty("lat", lat);
			en.setProperty("lon", lon);
			en.setProperty("creator", 1);
			data.put(en);
			
			out.write("SUCCESS");
		} else {
			out.write("ERROR");
		}
	}

}
