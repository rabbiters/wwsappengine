package org.cpe22.wws.appengine;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

@SuppressWarnings("serial")
public class Cal extends HttpServlet {
	
	private PrintWriter out;
	
	private String eventID = null;
	private DatastoreService data = DatastoreServiceFactory.getDatastoreService();
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;");
		resp.setCharacterEncoding("UTF-8");
		out = resp.getWriter();
		
		eventID = req.getParameter("eventID");
		if(eventID == null)
			return;
		
		Query queryEvent = new Query("eventLineItem");
		queryEvent.addFilter("eventID", FilterOperator.EQUAL, eventID);
		List<Entity> allEntities = data.prepare(queryEvent).asList(FetchOptions.Builder.withDefaults());
		if(allEntities.isEmpty())
			return;
			
		out.println("{");
		out.println("joiner:[");
		
		Query queryEventLineItem = new Query("eventLineItem");
		queryEventLineItem.addFilter("eventID", FilterOperator.EQUAL, eventID);
		List<Entity> allEntitiesLineItem = data.prepare(queryEventLineItem).asList(FetchOptions.Builder.withDefaults());
		for(int i=0;i<allEntitiesLineItem.size();i++){
			out.println("{");
			out.println("'lat':'"+allEntitiesLineItem.get(i).getProperty("lat")+"',");
			out.println("'lon':'"+allEntitiesLineItem.get(i).getProperty("lon")+"'");
			
			if(i != allEntitiesLineItem.size()-1)
				out.println("},");
			else
				out.println("}");
		}
		
		out.println("]");
		out.println("}");
	}
}
