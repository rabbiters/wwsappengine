package org.cpe22.wws.appengine;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

@SuppressWarnings("serial")
public class DelEvent extends HttpServlet {

	private DatastoreService data = DatastoreServiceFactory.getDatastoreService();
	
	private String eventID = null;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		eventID = req.getParameter("eventID");
		
		Query query = new Query("event");
		if(eventID != null)
			query.addFilter("eventID", FilterOperator.EQUAL, eventID);
		List<Entity> allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
		for(int i=0;i<allEntities.size();i++){
			data.delete(allEntities.get(i).getKey());
		}
		
		query = new Query("eventLineItem");
		if(eventID != null)
			query.addFilter("eventID", FilterOperator.EQUAL, eventID);
		allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
		for(int i=0;i<allEntities.size();i++){
			data.delete(allEntities.get(i).getKey());
		}
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		eventID = req.getParameter("eventID");
		
		Query query = new Query("event");
		if(eventID != null)
			query.addFilter("eventID", FilterOperator.EQUAL, eventID);
		List<Entity> allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
		for(int i=0;i<allEntities.size();i++){
			data.delete(allEntities.get(i).getKey());
		}
		
		query = new Query("eventLineItem");
		if(eventID != null)
			query.addFilter("eventID", FilterOperator.EQUAL, eventID);
		allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
		for(int i=0;i<allEntities.size();i++){
			data.delete(allEntities.get(i).getKey());
		}
	}

}
