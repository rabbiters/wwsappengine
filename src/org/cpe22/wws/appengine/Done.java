package org.cpe22.wws.appengine;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

@SuppressWarnings("serial")
public class Done extends HttpServlet {

	private DatastoreService data = DatastoreServiceFactory.getDatastoreService();
	
	private String eventID = null;
	private String uid = null;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		eventID = req.getParameter("eventID");
		uid = req.getParameter("uid");
		
		if(eventID == null && uid == null)
			return;
		
		Query query = new Query("eventLineItem");
		query.addFilter("eventID", FilterOperator.EQUAL, eventID);
		List<Entity> allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
		if(allEntities.size() == 1){
			query = new Query("event");
			query.addFilter("eventID", FilterOperator.EQUAL, eventID);
			allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
			for(int i=0;i<allEntities.size();i++){
				data.delete(allEntities.get(i).getKey());
			}
			
			query = new Query("place");
			query.addFilter("eventID", FilterOperator.EQUAL, eventID);
			allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
			for(int i=0;i<allEntities.size();i++){
				data.delete(allEntities.get(i).getKey());
			}
		}
		
		query = new Query("eventLineItem");
		query.addFilter("uid", FilterOperator.EQUAL, uid);
		allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
		for(int i=0;i<allEntities.size();i++){
			data.delete(allEntities.get(i).getKey());
		}
	}
	
	private PrintWriter out;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;");
		resp.setCharacterEncoding("UTF-8");
		out = resp.getWriter();
		
		eventID = req.getParameter("eventID");
		if(eventID == null)
			return;
		
		Query query = new Query("place");
		query.addFilter("eventID", FilterOperator.EQUAL, eventID);
		List<Entity> allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
		
		out.println("{eventID:'"+allEntities.get(0).getProperty("eventID")+"',");
		out.println("namePlace:'"+allEntities.get(0).getProperty("namePlace")+"',");
		out.println("address:'"+allEntities.get(0).getProperty("address")+"',");
		out.println("lat:"+allEntities.get(0).getProperty("lat")+",");
		out.println("lon:"+allEntities.get(0).getProperty("lon")+"}");
	}

}
