package org.cpe22.wws.appengine;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

@SuppressWarnings("serial")
public class Event extends HttpServlet {
	
	private PrintWriter out;
	
	private String eventID = null;
	private DatastoreService data = DatastoreServiceFactory.getDatastoreService();
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/html;");
		resp.setCharacterEncoding("UTF-8");
		out = resp.getWriter();
		/*get eventID from GET method*/
		eventID = req.getParameter("eventID");
		/*if no event id show other else*/
		if(eventID == null){
			noInfo();
			return;
		}
		
		/*----------query event detail----------*/
		/*event table*/
		Query queryEvent = new Query("event");
		queryEvent.addFilter("eventID", FilterOperator.EQUAL, eventID);
		List<Entity> allEntities = data.prepare(queryEvent).asList(FetchOptions.Builder.withDefaults());
		if(allEntities.isEmpty()){
			noInfo();
			return;
		}
		String fore_HTMLTAG = ""
				+ "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"+"\n"
				+ "<html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\" lang=\"th\">"+"\n"
				+ "<head prefix=\"og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# whenwhereshare: http://ogp.me/ns/fb/whenwhereshare#\">"+"\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"+"\n"
				+ "<title> When Where & Share </title>"+"\n"
				+ "  <meta property=\"fb:app_id\"      content=\"249069241853063\" /> "+"\n"
				+ "  <meta property=\"og:type\"        content=\"whenwhereshare:trip\" /> "+"\n"
				+ "  <meta property=\"og:url\"         content=\"http://whenwhereshare.appspot.com/event?eventID="+eventID+"\" /> "+"\n"
				+ "  <meta property=\"og:title\"       content=\""+allEntities.get(0).getProperty("title")+"\" /> "+"\n"
				+ "  <meta property=\"og:description\" content=\""
														+ allEntities.get(0).getProperty("name")+" has invited you for "
														+ "- " + allEntities.get(0).getProperty("title")+" -. Event start "
														+ allEntities.get(0).getProperty("date") + ","
														+" "+allEntities.get(0).getProperty("time")+".\" /> "+"\n"
				+ "  <meta property=\"og:image\"       content=\"http://qrcode.kaywa.com/img.php?s=6&d=http://whenwhereshare.appspot.com/event?eventID="+eventID+"\" />"+"\n" 
				+ "<style type=\"text/css\">"+"\n"
				+ "body,td,th {"+"\n"
				+	"color: #FFF;"+"\n"
				+	"font-weight: bold;"+"\n"
				+	"font-family: CordiaUPC;"+"\n"
				+	"text-align: center;"+"\n"
				+	"font-size: 22px;"+"\n"
				+ "}"+"\n"
				+ "body {"+"\n"
				+	"background-color: #666;"+"\n"
				+ "}"+"\n"
				+	".contents {" +"\n"
				+	"font-weight: normal;" +"\n"
				+	"font-size: 18px;" +"\n"
				+	"}" +"\n"
				+ "</style>"+"\n"
				+ "</head>"+"\n"
				+ "<body>"+"\n"
				+ "<div id=\"fb-root\"></div> "+"\n"
				+ "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+"\n"
		  		+ "<tr>"+"\n"
		  		+ "<td bgcolor=\"#FF6600\">Event Details </td>"+"\n"
		  		+ "</tr>"+"\n"
		  		+ "<tr>"+"\n"
		  		+ "<td bgcolor=\"#FFFFFF\"><center><img src=\"http://qrcode.kaywa.com/img.php?s=6&d=http://whenwhereshare.appspot.com/event?eventID="+eventID+"\" alt=\"qrcode\"  /></center></td>"+"\n"
		  		+ "</tr>"+"\n"
				+ "<tr>"+"\n"
				+ "    <td bgcolor=\"#FF6600\">Event Title</td>"+"\n"
				+ " </tr>"+"\n"
				+ "<tr>"+"\n"
				+ "    <td bgcolor=\"#FF9900\"><span class=\"contents\">"+allEntities.get(0).getProperty("title")+"</span></td>"+"\n"
				+ " </tr>"+"\n"
				+ "  <tr>"+"\n"
				+ "   <td bgcolor=\"#FF6600\">Description</td>"+"\n"
				+ "  </tr>"+"\n"
				+ "  <tr>"+"\n"
				+ "   <td bgcolor=\"#FF9900\"><span class=\"contents\">"+allEntities.get(0).getProperty("description")+"</span></td>"+"\n"
				+ "  </tr>"+"\n"
				+ "  <tr>"+"\n"
				+ "    <td bgcolor=\"#FF6600\">Date/Time</td>"+"\n"
				+ "  </tr>"+"\n"
				+ "  <tr>"+"\n"
				+ "    <td bgcolor=\"#FF9900\"><span class=\"contents\">"+allEntities.get(0).getProperty("date")+"   "+allEntities.get(0).getProperty("time")+"</span></td>"+"\n"
				+ "  </tr>"+"\n"
				+ "  <tr>"+"\n"
				+ "    <td bgcolor=\"#FF6600\">Event Creator</td>"+"\n"
				+ "  </tr>"+"\n"
				+ "  <tr>"+"\n"
				+ "    <td bgcolor=\"#FF9900\"><span class=\"contents\">"+allEntities.get(0).getProperty("name")+"</span></td>"+"\n"
				+ "  </tr>"+"\n"
				+ "  <tr>"+"\n"
				+ "   <td bgcolor=\"#FF6600\">Joiner</td></tr>";
		out.println(fore_HTMLTAG);		
		/*event line item*/
		Query queryEventLineItem = new Query("eventLineItem");
		queryEventLineItem.addFilter("eventID", FilterOperator.EQUAL, eventID);
		queryEventLineItem.addFilter("creator", FilterOperator.EQUAL, 0);
		List<Entity> allEntitiesLineItem = data.prepare(queryEventLineItem).asList(FetchOptions.Builder.withDefaults());
		if(allEntitiesLineItem.size() > 0){
			for(int i=0;i<allEntitiesLineItem.size();i++){
				out.println("<tr>");
				out.println("<td bgcolor=\"#FF9900\"><span class=\"contents\">");
				out.println(allEntitiesLineItem.get(i).getProperty("name"));
				out.println("</span></td></tr>");
			}
		}
		else{
			out.println("<tr>");
			out.println("<td bgcolor=\"#FF9900\"><span class=\"contents\">");
			out.println("No one joining");
			out.println("</span></td></tr>");
		}
		out.println("<tr>");
		out.println("<td bgcolor=\"#FF6600\">");
		out.println("<a name=\"fb_share\" type=\"button\""
        + "share_url=\"http://whenwhereshare.appspot.com/event?eventID="+eventID+"\">Share  this event on Facebook</a>" 
        + "<script src=\"http://static.ak.fbcdn.net/connect.php/js/FB.Share\"" 
        + "type=\"text/javascript\">"
        + "</script>");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");
		out.println("</body>");
		out.println("</html>");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;");
		resp.setCharacterEncoding("UTF-8");
		out = resp.getWriter();
		
		/*get eventID from GET method*/
		eventID = req.getParameter("eventID");
		
		/*if no event id show other else*/
		if(eventID == null){
			noInfo();
			return;
		}
		
		/*----------query event detail----------*/
		/*event table*/
		Query queryEvent = new Query("event");
		queryEvent.addFilter("eventID", FilterOperator.EQUAL, eventID);
		List<Entity> allEntities = data.prepare(queryEvent).asList(FetchOptions.Builder.withDefaults());
		if(allEntities.isEmpty()){
			out.print("EMPTY");
			return;
		} else {
			/*start JSON*/
			out.println("{");
		}
		
		out.println("'title':'"+allEntities.get(0).getProperty("title")+"',");
		out.println("'description':'"+allEntities.get(0).getProperty("description")+"',");
		out.println("'date':'"+allEntities.get(0).getProperty("date")+"',");
		out.println("'time':'"+allEntities.get(0).getProperty("time")+"',");
		out.println("'creator':'"+allEntities.get(0).getProperty("name")+"',");
		out.println("'started':'"+allEntities.get(0).getProperty("started")+"',");
		
		/*event line item*/
		out.println("'joiner':[");
		Query queryEventLineItem = new Query("eventLineItem");
		queryEventLineItem.addFilter("eventID", FilterOperator.EQUAL, eventID);
		queryEventLineItem.addFilter("creator", FilterOperator.EQUAL, 0);
		List<Entity> allEntitiesLineItem = data.prepare(queryEventLineItem).asList(FetchOptions.Builder.withDefaults());
		for(int i=0;i<allEntitiesLineItem.size();i++){
			out.println("{");
			out.println("'uid':'"+allEntitiesLineItem.get(i).getProperty("uid")+"',");
			out.println("'name':'"+allEntitiesLineItem.get(i).getProperty("name")+"'");
			
			if(i != allEntitiesLineItem.size()-1)
				out.println("},");
			else
				out.println("}");
		}
		out.println("]");
		
		/*end JSON*/
		out.println("}");
	}

	/*when no input eventID or eventID does not exist*/
	private void noInfo(){
		out.println("<center><h1>Welcome</h1><br>When Where Share</center>");
	}
}
