package org.cpe22.wws.appengine;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

@SuppressWarnings("serial")
public class JoinEvent extends HttpServlet {

	private PrintWriter out;

	private DatastoreService data = DatastoreServiceFactory.getDatastoreService();
	
	private String eventID = null;
	private String uid = null;
	private String name = null;
	private String lat = null;
	private String lon = null;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		out = resp.getWriter();
		resp.setContentType("text/html");
		
		out.write("<center><h1>Add Event</h1></center><br>");
		out.write("<center>Join event via mobile application only</center><br>");
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		out = resp.getWriter();
		resp.setContentType("text/html");

		eventID = req.getParameter("eventID");
		uid = req.getParameter("uid");
		name = req.getParameter("name");
		lat = req.getParameter("lat");
		lon = req.getParameter("lon");

		Query query = new Query("event");
		if(eventID == null)
			return;
		query.addFilter("eventID", FilterOperator.EQUAL, eventID);
		query.addFilter("started", FilterOperator.EQUAL, "1");
		List<Entity> allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
		if(allEntities.size() > 0){
			out.write("STARTED");
			return;
		}
		
		if(eventID!= null && uid!=null && name!=null && lat!=null && lon!=null){
			/*add into event line item*/
			Entity en = new Entity("eventLineItem");
			en.setProperty("eventID", eventID);
			en.setProperty("uid", uid);
			en.setProperty("name", name);
			en.setProperty("lat", lat);
			en.setProperty("lon", lon);
			en.setProperty("creator", 0);
			data.put(en);
			
			out.write("SUCCESS");
		} else {
			out.write("ERROR");
		}
	}

}
