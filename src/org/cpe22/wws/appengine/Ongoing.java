package org.cpe22.wws.appengine;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

@SuppressWarnings("serial")
public class Ongoing extends HttpServlet {
	
	private PrintWriter out;
	
	private String uid = null;
	private DatastoreService data = DatastoreServiceFactory.getDatastoreService();
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		out = resp.getWriter();
		resp.setContentType("text/html");
		
		uid = req.getParameter("uid");
		
		if(uid == null){
			noInfo();
			return;
		}

		Query queryEventLineItem = new Query("eventLineItem");
		queryEventLineItem.addFilter("uid", FilterOperator.EQUAL, uid);
		List<Entity> allEntitiesLineItem = data.prepare(queryEventLineItem).asList(FetchOptions.Builder.withDefaults());

		if(allEntitiesLineItem.size() == 0)
			out.println("0");
		else
			out.println(allEntitiesLineItem.get(0).getProperty("eventID"));
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		out = resp.getWriter();
		resp.setContentType("text/html");
		
		uid = req.getParameter("uid");
		
		if(uid == null){
			noInfo();
			return;
		}

		Query queryEventLineItem = new Query("eventLineItem");
		queryEventLineItem.addFilter("uid", FilterOperator.EQUAL, uid);
		List<Entity> allEntitiesLineItem = data.prepare(queryEventLineItem).asList(FetchOptions.Builder.withDefaults());

		if(allEntitiesLineItem.size() == 0){
			out.println("0");
		} else {
			Query query = new Query("event");
			query.addFilter("eventID", FilterOperator.EQUAL, allEntitiesLineItem.get(0).getProperty("eventID"));
			List<Entity> allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
			
			if(allEntities.get(0).getProperty("started").toString().compareTo("0") == 0){
				if(allEntitiesLineItem.get(0).getProperty("creator").toString().compareTo("1") == 0){
					out.println("z"+allEntitiesLineItem.get(0).getProperty("eventID"));
				} else {
					out.println(allEntitiesLineItem.get(0).getProperty("eventID"));
				}
			} else {
				out.println("x"+allEntitiesLineItem.get(0).getProperty("eventID"));
			}
		}
	}

	/*when no input eventID or eventID does not exist*/
	private void noInfo(){
		out.println("ERROR");
	}
}
