package org.cpe22.wws.appengine;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

@SuppressWarnings("serial")
public class Place extends HttpServlet {

private PrintWriter out;
	
	private String eventID = null;
	private String namePlace = null;
	private String lat = null;
	private String lon = null;
	private String address = null;
	private DatastoreService data = DatastoreServiceFactory.getDatastoreService();
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;");
		resp.setCharacterEncoding("UTF-8");
		out = resp.getWriter();
		
		eventID = req.getParameter("eventID");
		namePlace = req.getParameter("namePlace");
		lat = req.getParameter("lat");
		lon = req.getParameter("lon");
		address = req.getParameter("address");
		if(eventID == null && namePlace == null && lat == null && lon == null && address == null)
			return;
		
		Query query = new Query("event");
		query.addFilter("eventID", FilterOperator.EQUAL, eventID);
		List<Entity> allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
		data.delete(allEntities.get(0).getKey());
		
		Entity en = new Entity("event");
		en.setProperty("eventID", eventID);
		en.setProperty("title", allEntities.get(0).getProperty("title"));
		en.setProperty("creatorUID", allEntities.get(0).getProperty("creatorUID"));
		en.setProperty("name", allEntities.get(0).getProperty("name"));
		en.setProperty("date", allEntities.get(0).getProperty("date"));
		en.setProperty("time", allEntities.get(0).getProperty("time"));
		en.setProperty("description", allEntities.get(0).getProperty("description"));
		en.setProperty("started", 1);
		data.put(en);
		
		en = new Entity("place");
		en.setProperty("eventID", eventID);
		en.setProperty("namePlace", namePlace);
		en.setProperty("lat", lat);
		en.setProperty("lon", lon);
		en.setProperty("address", address);
		data.put(en);
	}
}
