package org.cpe22.wws.appengine;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

@SuppressWarnings("serial")
public class UnjoinEvent extends HttpServlet {

	private PrintWriter out;

	private DatastoreService data = DatastoreServiceFactory.getDatastoreService();
	
	private String eventID = null;
	private String uid = null;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		out = resp.getWriter();
		resp.setContentType("text/html");
		
		out.write("<center><h1>Add Event</h1></center><br>");
		out.write("<center>Add Event via mobile application only</center><br>");
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		out = resp.getWriter();
		resp.setContentType("text/html");

		eventID = req.getParameter("eventID");
		uid = req.getParameter("uid");
		
		if(eventID!= null && uid!=null){
			Query query = new Query("eventLineItem");
			query.addFilter("eventID", FilterOperator.EQUAL, eventID);
			query.addFilter("uid", FilterOperator.EQUAL, uid);
			
			List<Entity> allEntities = data.prepare(query).asList(FetchOptions.Builder.withDefaults());
			for(int i=0;i<allEntities.size();i++){
				data.delete(allEntities.get(i).getKey());
			}
			
			out.write("SUCCESS");
		} else {
			out.write("ERROR");
		}
	}

}
